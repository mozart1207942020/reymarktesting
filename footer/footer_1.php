<?php
global $porto_settings;
$footer_view = porto_get_meta_value( 'footer_view' );
$cols        = 0;

for ( $i = 1; $i <= 4; $i++ ) {
	if ( is_active_sidebar( 'footer-column-' . $i ) ) {
		$cols++;
	}
}
?>
<div id="footer" class="footer-1<?php echo ! $porto_settings['footer-ribbon'] ? '' : ' show-ribbon'; ?>"
<?php
if ( $porto_settings['footer-parallax'] ) {
	echo ' data-plugin-parallax data-plugin-options="{&quot;speed&quot;: ' . esc_attr( $porto_settings['footer-parallax-speed'] ) . '}" style="background-image: none !important;"';}
?>
>
	<?php if ( ! $footer_view && $cols ) : ?>
		<div class="footer-main">
			<div class="container">
				<?php if ( $porto_settings['footer-ribbon'] ) : ?>
					<div class="footer-ribbon"><?php echo wp_kses_post( $porto_settings['footer-ribbon'] ); ?></div>
				<?php endif; ?>

				<?php
				if ( $cols ) :
					$col_class = array();
					switch ( $cols ) {
						case 1:
							$col_class[1] = 'col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget1'] ) ? $porto_settings['footer-widget1'] : '12' );
							break;
						case 2:
							$col_class[1] = 'col-sm-12 col-md-4 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget1'] ) ? $porto_settings['footer-widget1'] : '6' );
							$col_class[2] = 'col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget2'] ) ? $porto_settings['footer-widget2'] : '6' );
							break;
						case 3:
							$col_class[1] = 'col-sm-6 col-md-6 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget1'] ) ? $porto_settings['footer-widget1'] : '4' );
							$col_class[2] = 'col-sm-6 col-md-6 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget2'] ) ? $porto_settings['footer-widget2'] : '4' );
							$col_class[3] = 'col-sm-12 col-md-12 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget3'] ) ? $porto_settings['footer-widget3'] : '4' );
							break;
						case 4:
							$col_class[1] = 'col-6 col-sm-6 col-md-3 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget1'] ) ? $porto_settings['footer-widget1'] : '3' );
							$col_class[2] = 'col-6 col-sm-6 col-md-3 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget2'] ) ? $porto_settings['footer-widget2'] : '3' );
							$col_class[3] = 'col-6 col-sm-6 col-md-3 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget3'] ) ? $porto_settings['footer-widget3'] : '3' );
							$col_class[4] = 'col-6 col-sm-6 col-md-3 col-lg-' . ( ( $porto_settings['footer-customize'] && $porto_settings['footer-widget4'] ) ? $porto_settings['footer-widget4'] : '3' );
							break;
					}
					?>
					<!-- <div class="row">
						<?php
						// $cols = 1;
						// for ( $i = 1; $i <= 4; $i++ ) {
						// 	if ( is_active_sidebar( 'footer-column-' . $i ) ) {
								?>
								<div class="fc-<?php //echo $i; ?> <?php //echo esc_attr( $col_class[ $cols++ ] ); ?>">
									<?php //dynamic_sidebar( 'footer-column-' . $i ); ?>
								</div>
								<?php
						// 	}
						// }
						?>
					</div> -->

					<div class="row">
					    <div class="fc-1 col-12 col-sm-6 col-md-2 col-lg-2">
					        <aside id="text-9" class="widget widget_text">
					            <h3 class="widget-title">NAVIGATION</h3>
					            <div class="textwidget">
					                    <a class="footer-link" href="https://blosumcbd.com/about-us/">About Us</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/our-story/">Our Story</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/contact-us/">Contact Us</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/contact-us/">Wholesale</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/affiliates/">Affiliates</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/cbd-near-me/">CBD Near Me</a><br />
					                    <a href="https://blosumcbd.com/cbd-101/"><span class="footer-link">CBD 101</span></a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/lab-test-report/">Third Party Lab Testing</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/privacy-policy/">Privacy Policy</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/terms-of-service/">Terms of Service</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/shipping-and-returns-policy/">Shipping &amp; Returns Policy</a><br />
					                    <a class="footer-link" href="https://blosumcbd.com/sitemap/">Sitemap</a><br />
					            </div>
					        </aside>
					    </div>
					    <div class="fc-2 col-12 col-sm-6 col-md-2 col-lg-2">
					        <aside id="text-10" class="widget widget_text">
					            <h3 class="widget-title"><a href="https://blosumcbd.com/products/">CBD PRODUCTS</a></h3>
					            <div class="textwidget">
					                    <a href="https://blosumcbd.com/products/"><span class="footer-link">CBD for Sale</span></a><br />
					                    <a href="https://blosumcbd.com/product/cbd-capsule-softgels-25mg/"><span class="footer-link">CBD Capsules</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-gummies/"><span class="footer-link">CBD Gummies</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-oils/"><span class="footer-link">CBD Tincture</span></a><br />
					                    <a href="https://blosumcbd.com/product/cbd-salve-500mg/"><span class="footer-link">CBD Salve </span></a><br />
					                    <a href="https://blosumcbd.com/product/dog-treat-soft-chews-3mg/"><span class="footer-link">CBD for Pets </span></a>
					                <p>&nbsp;</p>
					            </div>
					        </aside>
					    </div>
					    <div class="fc-3 col-12  col-sm-12  col-xs-12  col-md-2 col-lg-2">
					        <aside id="text-11" class="widget widget_text">
					            <h3 class="widget-title">CBD NEWS</h3>
					            <div class="textwidget">
					                    <a href="https://blosumcbd.com/blog/hemp-oil-vs-cbd-oil/"><span class="footer-link">Hemp Oil vs CBD</span></a><br />
					                    <a href="https://blosumcbd.com/blog/how-much-cbd-should-i-take/"><span class="footer-link">How Much CBD Should I Take?</span></a><br />
					                    <a href="https://blosumcbd.com/blog/full-spectrum-vs-broad-spectrum-cbd/"><span class="footer-link">FullL spectrum VS Broad spectrum CBD</span></a><br />
					                    <a href="https://blosumcbd.com/blog/is-cbd-oil-right-for-me/"><span class="footer-link">Is CBD Oil Right for Me?</span></a><br />
					                    <a href="https://blosumcbd.com/blog/premium-quality-cbd/"><span class="footer-link">Premium Quality CBD</span></a><br>
					                    <a href="https://blosumcbd.com/blog/cbd-recipes-you-need-to-try/"><span class="footer-link">CBD Recipes You Need to Try</span></a>
					            </div>
					        </aside>
					    </div>
					    <div class="fc-3 col-12  col-sm-12  col-xs-12 col-md-2 col-lg-2">
					        <aside id="text-11" class="widget widget_text">
					            <h3 class="widget-title"><a href="https://blosumcbd.com/cbd-near-me/">CBD NEAR ME</a></h3>
					            <div class="textwidget">
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-california/"><span class="footer-link">CBD California</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-colorado/"><span class="footer-link">CBD Colorado</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-florida/"><span class="footer-link">CBD Florida</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-idaho/"><span class="footer-link">CBD Idaho</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-newyork/"><span class="footer-link">CBD New York</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-north-carolina/"><span class="footer-link">CBD North Carolina</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-ohio/"><span class="footer-link">CBD Ohio</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-oregon/"><span class="footer-link">CBD Oregon</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-texas/"><span class="footer-link">CBD Texas</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-virginia/"><span class="footer-link">CBD Virginia</span></a><br />
					                    <a href="https://blosumcbd.com/cbd-near-me/cbd-wisconsin/"><span class="footer-link">CBD Wisconsin</span></a><br />
					            </div>
					        </aside>
					    </div>
					    <div class="fc-4 col-12 col-sm-12  col-xs-12 col-md-4 col-lg-4">
					        <aside id="custom_html-3" class="widget_text widget widget_custom_html">
					            <h3 class="widget-title">CONTACT US</h3>
					            <div class="textwidget custom-html-widget" itemscope itemtype="http://schema.org/Organization">
					                <p style="margin-bottom: 5px;">
					                    Email: <a href="mailto:support@blosumcbd.com"><span itemprop="email" style="font-size: 12px !important;">support@blosumcbd.com</span></a>
					                </p>
					                <p style="margin-bottom: 5px;">
					                    Phone: <a href="tel:888-938-7837"><span itemprop="telephone" style="font-size: 12px !important;">+1 888-938-7837</span></a>
					                </p>
					                <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="textwidget">
					                    <p class="address" style="color: #fff !important;">
					                        Address:
					                        <a href="https://goo.gl/maps/jAYubxuRMKD3Nrhf8" target="_blank" rel="noopener noreferrer">
					                            <span itemprop="streetAddress" style="font-size: 12px !important;">4630 campus dr. Suite 200i</span> <span itemprop="addressLocality" style="font-size: 12px !important;">Newport Beach</span>, <span itemprop="addressRegion" style="font-size: 12px !important;">CA </span><span itemprop="postalCode" style="font-size: 12px !important;">92660</span>,
					                            <span itemprop="addressCountry" style="font-size: 12px !important;">USA</span>
					                        </a>
					                    </p>
					                </div>
					                <a href="https://goo.gl/maps/jAYubxuRMKD3Nrhf8" target="_blank" style="font-size: 12px !important;" rel="noopener noreferrer">
					                	<img src="https://blosumcbd.com/wp-content/uploads/2020/05/blosumCBD-Location-Map.jpg" alt="BlosumCBD Location Map" class="img-responsive">
					                </a>
					            </div>

					        </aside>
					        <aside id="follow-us-widget-4" class="widget follow-us">
					            <h3 class="widget-title">Follow Us</h3>
					            <div class="share-links disable-br">
					                <a href="https://www.facebook.com/BlosumCBD/" rel="nofollow" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" class="share-facebook" data-original-title="Facebook">Facebook</a>
					                <a href="https://twitter.com/BlosumCbd" rel="nofollow" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" class="share-twitter" data-original-title="Twitter">Twitter</a>
					                <a href="https://www.pinterest.com/blosumcbd/" rel="nofollow" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" class="share-pinterest" data-original-title="Pinterest">Pinterest</a>
					                <a href="https://www.instagram.com/blosumcbd" rel="nofollow" target="_blank" data-toggle="tooltip" data-placement="bottom" title="" class="share-instagram" data-original-title="Instagram">Instagram</a>
					            </div>
					        </aside>
					    </div>
					</div>


				<?php endif; ?>

				<?php
				get_template_part( 'footer/footer_tooltip' );
				?>
			</div>
		</div>
	<?php endif; ?>

	
</div>
