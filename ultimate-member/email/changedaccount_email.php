<div style="max-width: 600px; background: #ffffff; border-radius: 5px; margin: 40px auto 0; font-family: Helvetica, Roboto, Arial, sans-serif; font-size: 15px; color: #666;">
<div style="background-image: url('https://blosumcbd.com/wp-content/uploads/2020/02/email-template-head.png'); background-size: contain; background-repeat: no-repeat; height: 120px;"> </div>
<h1 style="text-align: center; font-size: 22px; font-weight: 300; color: #202020;">You recently updated your account.</h1>
<div style="padding: 0 30px 0 30px;">
<div style="padding: 10px 0 0 0; text-align: center;"><a style="background: #555555; color: #fff; padding: 12px 30px; text-decoration: none; border-radius: 3px; letter-spacing: 0.3px;" href="{user_account_link}">Go to your Account</a></div>
<div style="padding: 30px 0 50px 0; text-align: center;">If you did not make this change and believe your account has been compromised, please <a style="color: #3ba1da; text-decoration: none;" href="mailto:{admin_email}">contact us</a> ASAP.</div>
</div>
</div>
<div style="max-width: 600px; padding: 20px 0; background: #000000; margin: 0 auto; font-family: Open Sans,Helvetica,Arial; font-size: 15px; color: #666;">
<div style="display: inline-block; width: 40%; text-align: center;"><a href="https://blosumcbd.com"> <img src="https://blosumcbd.com/wp-content/uploads/2020/01/blosum-cbd-logo.png" width="100" /> </a></div>
<div style="float: right; margin-top: 20px; width: 60%; text-align: right;"><a style="margin-right: 10px;"> <img src="https://blosumcbd.com/wp-content/uploads/2019/12/fb-icon-w.png" width="25" /> </a> <a style="margin-right: 10px;"> <img src="https://blosumcbd.com/wp-content/uploads/2019/12/inst-icon-w.png" width="25" /> </a> <a style="margin-right: 10px;"> <img src="https://blosumcbd.com/wp-content/uploads/2019/12/twtr-icon-w.png" width="25" /> </a> <a style="margin-right: 10px;"> <img src="https://blosumcbd.com/wp-content/uploads/2019/12/yelp.png" width="25" /> </a></div>
	<p style="width: 100%; text-align: center; color: #fff; font-size: 12px;"><a href="https://blosumcbd.com/privacy-policy/" style="color: #fff; font-size: 12px;">Privacy Policy</a> | <a href="https://blosumcbd.com/terms-of-service/" style="color: #fff; font-size: 12px;">Terms of Service</a></p>
<p style="width: 100%; text-align: center; color: #fff; font-size: 12px;">All rights reserved <a style="color: #fff; font-size: 12px;">Blosum CBD</a> © 2020</p>
</div>