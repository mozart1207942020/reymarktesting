<!DOCTYPE html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta name="ahrefs-site-verification" content="6890f66bcde35fc14b01f0b5cc13d552e4f333c8f140ab2b59fc2f2ce98725eb">
	<meta name="p:domain_verify" content="c8602d042005c53d54e0b9b53bb83284"/>
	<meta name="yandex-verification" content="8d422e39a468bfc6" />
	<meta name="msvalidate.01" content="28F85D6087D2D3716FB3D1BD6AD8E5A9" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php get_template_part( 'head' ); ?> 
	
</head>
<?php
$pid = get_the_ID();
global $porto_settings;
$wrapper     = porto_get_wrapper_type();
$body_class  = $wrapper;
$body_class .= ' blog-' . get_current_blog_id();
$body_class .= ' ' . $porto_settings['css-type'];

$header_is_side = porto_header_type_is_side();

if ( $header_is_side ) {
	$body_class .= ' body-side';
}

$loading_overlay = porto_get_meta_value( 'loading_overlay' );
$showing_overlay = false;
if ( 'no' !== $loading_overlay && ( 'yes' === $loading_overlay || ( 'yes' !== $loading_overlay && $porto_settings['show-loading-overlay'] ) ) ) {
	$showing_overlay = true;
	$body_class     .= ' loading-overlay-showing';
}

?>


<!-- Taboola Pixel Code -->
<!-- <script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1305473});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1305473/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='https://trc.taboola.com/1305473/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript> -->
<!-- End of Taboola Pixel Code -->

<body <?php body_class( array( $body_class ) ); ?><?php echo ! $showing_overlay ? '' : ' data-loading-overlay'; ?>>
	
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N4D6N82" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <!-- End Google Tag Manager (noscript) -->
<noscript><img height="1" width="1" alt="fb" style="display:none" src="https://www.facebook.com/tr?id=212974009889837&ev=PageView&noscript=1"/></noscript>
<noscript><img height="1" width="1" style="display:none" src="https://track.adtrue.com/px?id=69548246&ev=PageView&noscript=1"/></noscript>



<?php if( is_front_page() ): ?> 
	<div class="body-bg"></div>
<?php endif; ?>
<?php if ( $showing_overlay ) : ?>
	<div class="loading-overlay">
		<div class="bounce-loader">
			<div class="bounce1"></div>
			<div class="bounce2"></div>
			<div class="bounce3"></div>
		</div>
	</div>
	<?php
endif;

	// Get Meta Values
	wp_reset_postdata();
	global $porto_layout, $porto_sidebar;

	$porto_layout_arr = porto_meta_layout();
	$porto_layout     = $porto_layout_arr[0];
	$porto_sidebar    = $porto_layout_arr[1];
if ( in_array( $porto_layout, porto_options_both_sidebars() ) ) {
	$GLOBALS['porto_sidebar2'] = $porto_layout_arr[2];
}

	$porto_banner_pos = porto_get_meta_value( 'banner_pos' );

if ( porto_show_archive_filter() ) {
	if ( 'fullwidth' == $porto_layout ) {
		$porto_layout = 'left-sidebar';
	}
	if ( 'widewidth' == $porto_layout ) {
		$porto_layout = 'wide-left-sidebar';
	}
}

	$breadcrumbs       = $porto_settings['show-breadcrumbs'] ? porto_get_meta_value( 'breadcrumbs', true ) : false;
	$page_title        = $porto_settings['show-pagetitle'] ? porto_get_meta_value( 'page_title', true ) : false;
	$content_top       = porto_get_meta_value( 'content_top' );
	$content_inner_top = porto_get_meta_value( 'content_inner_top' );

	
if ( is_front_page() ) {
	$breadcrumbs = false;
	$page_title  = false;
}
else if(!is_singular('product')){
	$page_title  = false;
	$breadcrumbs = true;
}
else if(is_singular('product')){
	$page_title  = true;
	$breadcrumbs = false;
}
//is_page_template('/page-custom-layout.php') || ( (is_page() && !is_front_page()) || in_array(get_post_type(), ['post', 'product']) ) && 

	do_action( 'porto_before_wrapper' );
?>

	<div class="page-wrapper<?php echo ! $header_is_side ? '' : ' side-nav', isset( $porto_settings['header-side-position'] ) && $porto_settings['header-side-position'] ? ' side-nav-right' : ''; ?>"><!-- page wrapper -->

		<?php
		if ( 'before_header' == $porto_banner_pos ) {
			porto_banner( 'banner-before-header' );
		}
			do_action( 'porto_before_header' );

			$header_wrapper_class_escaped = 'header-wrapper';
		if ( 'wide' == $porto_settings['header-wrapper'] ) {
			$header_wrapper_class_escaped .= ' wide';
		}
		if ( 'reveal' == $porto_settings['sticky-header-effect'] ) {
			$header_wrapper_class_escaped .= ' header-reveal';
		}
		if ( ! ( $header_is_side && 'boxed' == $wrapper ) && ( 'below_header' == $porto_banner_pos || 'fixed' == $porto_banner_pos || porto_get_meta_value( 'header_view' ) == 'fixed' ) || 'fixed' == $porto_settings['header-view'] ) {
			$header_wrapper_class_escaped .= ' fixed-header';
			if ( $porto_settings['header-fixed-show-bottom'] ) {
				$header_wrapper_class_escaped .= ' header-transparent-bottom-border';
			}
		}
		if ( $header_is_side ) {
			$header_wrapper_class_escaped .= ' header-side-nav';
		}
		?>

		<?php if ( porto_get_meta_value( 'header', true ) && 'hide' != $porto_settings['header-view'] ) : ?>
			<!-- header wrapper -->
			<div class="<?php echo esc_attr( $header_wrapper_class_escaped ); ?>">
				<?php if ( porto_get_wrapper_type() != 'boxed' && 'boxed' == $porto_settings['header-wrapper'] ) : ?>
				<div id="header-boxed">
				<?php endif; ?>
				<?php
					get_template_part( 'header/header' );
				?>

				<?php if ( porto_get_wrapper_type() != 'boxed' && 'boxed' == $porto_settings['header-wrapper'] ) : ?>
				</div>
				<?php endif; ?>
			</div>
			<!-- end header wrapper -->
		<?php endif; ?>

		<?php if ( 'side' == porto_get_header_type() ) : ?>
			<div class="content-wrapper">
		<?php endif; ?>

		<?php
			do_action( 'porto_before_banner' );
		if ( 'before_header' != $porto_banner_pos ) {
			porto_banner( ( 'fixed' == $porto_banner_pos && 'boxed' !== $wrapper ) ? 'banner-fixed' : '' );
		}

			do_action( 'porto_before_breadcrumbs' );
			get_template_part( 'breadcrumbs' );
			do_action( 'porto_before_main' );

			$main_class         = array();
			$main_content_class = array( 'main-content' );

		if ( in_array( $porto_layout, porto_options_both_sidebars() ) ) {
			$main_class[]   = 'column3';
			$mobile_sidebar = porto_get_meta_value( 'mobile_sidebar' );
			if ( 'yes' == $mobile_sidebar ) {
				$mobile_sidebar = true;
			} elseif ( 'no' == $mobile_sidebar ) {
				$mobile_sidebar = false;
			} else {
				$mobile_sidebar = $porto_settings['show-mobile-sidebar'];
			}
			if ( $mobile_sidebar ) {
				$main_content_class[] = 'col-md-8 col-lg-6';
			} else {
				$main_content_class[] = 'col-lg-6';
			}
		} elseif ( in_array( $porto_layout, porto_options_sidebars() ) ) {
			$main_class[]         = 'column2';
			$main_class[]         = 'column2-' . str_replace( 'wide-', '', $porto_layout );
			$main_content_class[] = 'col-lg-9';
		} else {
			$main_class[]         = 'column1';
			$main_content_class[] = 'col-lg-12';
		}

		if ( porto_is_wide_layout( $porto_layout ) ) {
			$main_class[] = 'wide clearfix';
		} else {
			$main_class[] = 'boxed';
		}
		if ( ! $breadcrumbs && ! $page_title ) {
			$main_class[] = 'no-breadcrumbs';
		}
		if ( porto_get_wrapper_type() != 'boxed' && 'boxed' == $porto_settings['main-wrapper'] ) {
			$main_class[] = 'main-boxed';
		}
		?>

		<div id="main" class="<?php echo esc_attr( implode( ' ', $main_class ) ); ?>"><!-- main -->

			<?php
				do_action( 'porto_before_content_top' );
			if ( $content_top ) :
				?>
					<div id="content-top"><!-- begin content top -->
						<?php
						foreach ( explode( ',', $content_top ) as $block ) {
							echo do_shortcode( '[porto_block name="' . esc_attr( $block ) . '"]' );
						}
						?>
					</div><!-- end content top -->
				<?php
				endif;
				do_action( 'porto_after_content_top' );

			if ( 'boxed' == $wrapper || 'fullwidth' == $porto_layout || 'left-sidebar' == $porto_layout || 'right-sidebar' == $porto_layout || 'both-sidebar' == $porto_layout ) {
				if( $pid == '2534' || is_page_template( 'page-custom-layout.php' ) ) {
					echo '<div class="container-fluid">';
				} else{
					echo '<div class="container">';

				}
				if ( class_exists( 'WC_Vendors' ) ) {
					porto_wc_vendor_header();
				}
			} else {
				echo '<div class="container-fluid">';
			}

				do_action( 'porto_before_content' );

				global $porto_shop_filter_layout;
			?>

			<div class="row main-content-wrap<?php echo isset( $porto_shop_filter_layout ) && 'horizontal' == $porto_shop_filter_layout ? ' porto-products-filter-body' : ''; ?>">

			<!-- main content -->
			<div class="<?php echo esc_attr( implode( ' ', $main_content_class ) ); ?>">

			<?php
				wp_reset_postdata();
				do_action( 'porto_before_content_inner_top' );
			if ( $content_inner_top ) :
				?>
					<div id="content-inner-top"><!-- begin content inner top -->
					<?php
					foreach ( explode( ',', $content_inner_top ) as $block ) {
						echo do_shortcode( '[porto_block name="' . esc_attr( $block ) . '"]' );
					}
					?>
					</div><!-- end content inner top -->
				<?php endif;
				do_action( 'porto_after_content_inner_top' );
			?>
