<?php
header("HTTP/1.0 404 Not Found");
get_header();

global $porto_settings;
?>

<div id="content" class="no-content">
	<div class="container">
		<section class="page-not-found">
			<div class="row">
				<div class="col-12">
					<img src="https://blosumcbd.com/wp-content/uploads/2019/12/404-im.png">
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<h1 style="margin-top: 10px;margin-bottom: 10px;">
						<span style="font-size: 30px;">Oops! Page not found</span>
					</h1>
					<p><span style="font-size: 18px;">you may continue searching</span></p>
					<p><a style="color: #b18708; font-size: 18px; text-decoration: none;" href="/">Home</a>&nbsp; |&nbsp; <a style="color: #b18708; font-size: 18px; text-decoration: none;" href="/blosum-nation/">Blosum Nation</a> | <a style="color: #b18708; font-size: 18px; text-decoration: none;" href="/products/">Products</a></p>
				</div>
			</div>

		</section>
	</div>
</div>

<?php get_footer(); ?>
