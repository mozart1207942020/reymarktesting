<?php get_header(); ?>

<?php
global $porto_settings, $porto_layout;

$featured_images = porto_get_featured_images();
?>
	<div id="content" role="main">
		<?php /* The loop */ ?>
		<?php
		while ( have_posts() ) :
			the_post();
			the_content();
			?>

			

		<?php endwhile; ?>

	</div>

<?php get_footer(); ?>
