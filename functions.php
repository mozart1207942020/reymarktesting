<?php
//Do not remove Uppercase redirects
include 'inc/uppercase_redirects.php';
include 'inc/entex_add_crumb_schema.php';

if ( !is_front_page()){
    include 'inc/table_of_contents_shortcode.php';
}




add_action( 'wp_enqueue_scripts', 'porto_child_css', 1001 );
// add_action( 'get_footer', 'porto_child_css', 1001 );

ini_set('max_execution_time', 600);

// Load CSS
function porto_child_css() {
    // porto child theme styles
    wp_deregister_style( 'styles-child' );
    wp_register_style( 'styles-child', esc_url( get_stylesheet_directory_uri() ) . '/style.css' );
    wp_enqueue_style( 'styles-child' );

    if ( is_rtl() ) {
        wp_deregister_style( 'styles-child-rtl' );
        wp_register_style( 'styles-child-rtl', esc_url( get_stylesheet_directory_uri() ) . '/style_rtl.css' );
        wp_enqueue_style( 'styles-child-rtl' );
    }
}



function add_categories_after_title( $title, $id ) {
    global $post;
    if ( is_front_page( ) && $post->post_type == 'post' ) {  // it will work only for posts on single page, but you can modify this condition
        $categories = get_the_category();
        $cats_string = '';
        if ( ! empty($categories) ) {
            foreach ( $categories as $term ) {
                $cats_string .= $term->name . ' ';
            }
        }
        if ( $cats_string ) {
            $title .= '<br><span>' . $cats_string .'</span>';
        }

        $title = '<p>' . $title .'</p>';
    }

    return $title; 
}

add_filter( 'the_title', 'add_categories_after_title', 10, 2 );

function add_save_percentage_after_price( $price, $product ) {
    $product_sale_price = $product->get_sale_price();
    $product_regular_price = $product->get_regular_price();
    if( $product_sale_price > 0 ) {
        $percentage = round( ( ( $product_regular_price - $product_sale_price ) / $product_regular_price ) * 100 );
        return $price .'<span class="save_per">'. sprintf( __('Save %s', 'woocommerce' ), $percentage . '%' ) .'</span>';
    } else {
        return $price;
    }
}
add_filter( 'woocommerce_get_price_html', 'add_save_percentage_after_price', 10, 2 );


function woocommerce_single_product_image_thumbnail_html( $html, $attachment_id ) {
    global $product;
    $attachment_ids = $product->get_gallery_image_ids();

    return ( $attachment_ids && $product->get_image_id() == $attachment_id ) ? '' : $html;
}

add_filter( 'woocommerce_product_tabs', 'remove_info_tab', 98);
function remove_info_tab($tabs) {
 
    unset($tabs['additional_information']);     
     return $tabs;
}

// Add image after the add to cart button

add_action ( 'woocommerce_single_product_summary', 'add_img_after_atc', 40 );
function add_img_after_atc() {
    global $product;
    print '<img src="https://blosumcbd.com/wp-content/uploads/2019/10/mcafee2.png" alt="your-image" class="atc_img" />';
}


add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
    global $product;    
    $product_type = $product->get_type();  
    return __( 'Explore Options', 'woocommerce' );
} 


add_action( 'woocommerce_after_add_to_cart_button', 'add_content_after_addtocart_button_func' );
/*
 * Content below "Add to cart" Button.
 */
function add_content_after_addtocart_button_func() {

        // Echo content.
        echo '<a class="learn-more" href="">Learn More</a>';

}

add_action('woocommerce_after_shop_loop_item_title', 'add_new_atc');
function add_new_atc()
{
    global $woocommerce, $product;

    global $post;
    $post_slug = $post->post_name;
?>

    <?php if( is_page('auto-ship-subscription') ): $url = ( is_user_logged_in() ? wc_get_page_permalink( 'checkout' ) : home_url('/register/')); ?>

        <form class="cart" action="<?php echo $url; ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="convert_to_sub_<?php echo $product->get_id(); ?>" data-custom_data='{"subscription_scheme":{"period":"month","interval":1,"length":0,"trial_period":"","trial_length":"","pricing_mode":"inherit","discount":"20","sync_date":0,"context":"product","id":"1_month","key":"1_month","is_synced":false,"is_prorated":false},"overrides_price":true,"discount_from_regular":false}' value="1_month">
            <input type="hidden" name="quantity" value="1">
            <input type="hidden" name="add-to-cart" value="<?php echo $product->get_id(); ?>">
            <div class="add-links new-al">
                <a class="button add_to_cart_button ajax_add_to_cart new_atc addtosubs" rel="nofollow">Add to cart</a>
            </div>
        </form>

    <?php else: ?>

        <script>
            function add_to_cart_track_ob(){
                obApi('track', '<?php echo "atc_".$post_slug ?>');
            }
        </script>
        <div class="add-links new-al">
            <a href="?add-to-cart=<?php echo $product->get_id(); ?>" onclick="add_to_cart_track_ob()" data-quantity="1" class="button add_to_cart_button ajax_add_to_cart new_atc" data-product_id="<?php echo $product->get_id(); ?>" rel="noindex nofollow">Add to cart</a>
        </div>

    <?php endif; ?>

<?php        
}

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

remove_action( 'woocommerce_shop_loop_item_title','woocommerce_template_loop_product_title', 10 );

/*add_action( 'wp_footer', 'trigger_for_ajax_add_to_cart' );
function trigger_for_ajax_add_to_cart() {
    ?>
        <script type="text/javascript">
            (function($){
                $('body').on( 'added_to_cart', function(){
                    if( $('body').hasClass('page-id-3728') ) {
                        <?php if( is_user_logged_in() ): ?>
                            window.location.href = '/checkout/';
                        <?php else: ?>
                            window.location.href = '/register/';
                        <?php endif; ?>
                    }
                    // Your code goes here
                });
            })(jQuery);
        </script>
    <?php
}*/

/* Redirect to shop after login.
 */
/*function iconic_login_redirect( $redirect, $user ) {
    $redirect_page_id = url_to_postid( $redirect );
    $checkout_page_id = wc_get_page_id( 'checkout' );
    
    if( ! WC()->cart->is_empty()  ) {
       return wc_get_page_permalink( 'checkout' );
    }
 
    return wc_get_page_permalink( 'shop' );
}
 
add_filter( 'woocommerce_login_redirect', 'iconic_login_redirect' );*/

add_action( 'wp', 'redirect' );
function redirect() {
  if ( is_page('my-account') && !is_user_logged_in() ) {
      wp_redirect( home_url('/sign-in/') );
      die();
  }
  if ( is_page('register') || is_page('sign-in')  && is_user_logged_in() ) {
      wp_redirect( home_url('/my-account/orders') );
      die();
  }
}

// alter the credit card postal error
function my_woocommerce_add_error( $error ) {
    if( "The postal code in billing address doesn't match the one used for card nonce creation." == $error ) {
        $error = "Zipcode doesn't match your billing address. Please try a correct zipcode.";
    }
    return $error;
}
add_filter( 'woocommerce_add_error', 'my_woocommerce_add_error' );

add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    add_image_size( 'homepage-thumb-p', 148); // (cropped)
}

/** Disable Ajax Call from WooCommerce on front page and posts*/
/*add_action( 'wp_enqueue_scripts', 'dequeue_woocommerce_cart_fragments', 11);
function dequeue_woocommerce_cart_fragments() {
if (is_front_page() || is_single() ) wp_dequeue_script('wc-cart-fragments');
}*/




/****** REYMARK ADDITIONAL FUNCTION *******/

add_filter( 'woocommerce_get_order_item_totals', 'add_coupons_codes_line_to_order_totals_lines', 10, 3 );
function add_coupons_codes_line_to_order_totals_lines( $total_rows, $order, $tax_display ) {
    // Exit if there is no coupons applied
    if( sizeof( $order->get_coupon_codes() ) == 0 )
        return $total_rows;

    $new_total_rows = []; // Initializing

    foreach($total_rows as $key => $total ){
        $new_total_rows[$key] = $total;

        if( $key == 'discount' ){
            // Get applied coupons
            $applied_coupons = $order->get_coupon_codes();
            // Insert applied coupon codes in total lines after discount line
            $new_total_rows['coupon_codes'] = array(
                'label' => __('Applied coupons:', 'woocommerce'),
                'value' => implode( ', ', $applied_coupons ),
            );
        }
    }

    return $new_total_rows;
}

/* ADD Featured image into rss feed*/
function addRssFeaturedImage() {
  global $post;
  if (has_post_thumbnail($post->ID)) {
    echo '<featured_image>' . wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured_size' )[0] . '</featured_image>';
  }
}
add_action('rss2_item', 'addRssFeaturedImage');


/* Disable Email triggers when Adding manual order */

add_action( 'woocommerce_email', 'unhook_those_emails' );

function unhook_those_emails( $email_class ) {

    if ( is_admin() && ! wp_doing_ajax() ) {

        // New order emails
        remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        remove_action( 'woocommerce_order_status_pending_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        remove_action( 'woocommerce_order_status_failed_to_processing_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        remove_action( 'woocommerce_order_status_failed_to_completed_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );
        remove_action( 'woocommerce_order_status_failed_to_pending_notification', array( $email_class->emails['WC_Email_New_Order'], 'trigger' ) );

        // Processing order emails
        remove_action( 'woocommerce_order_status_pending_to_processing_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );
        remove_action( 'woocommerce_order_status_pending_to_on-hold_notification', array( $email_class->emails['WC_Email_Customer_Processing_Order'], 'trigger' ) );

        // Completed order emails
        remove_action( 'woocommerce_order_status_completed_notification', array( $email_class->emails['WC_Email_Customer_Completed_Order'], 'trigger' ) );

    }
}

/* REGISTER HTML5 THEME SUPPORT */
add_action(
    'after_setup_theme',
    function() {
        add_theme_support( 'html5', [ 'script', 'style' ] );
    }
);

add_filter('style_loader_tag', 'codeless_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'codeless_remove_type_attr', 10, 2);
function codeless_remove_type_attr($tag, $handle) {
    return preg_replace( "/type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}


// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' . get_bloginfo( 'version' ) ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );


//adding required meta tag for amp page - rel canonical
// add_action( 'amp_post_template_head', function(){
//     echo rel_canonical(); 
// } );


add_filter( 'woocommerce_coupon_error','coupon_error_message_change',10,3 );

function coupon_error_message_change($err, $err_code, $parm ){
    switch ( $err_code ) {
        
        case 105:
        /* translators: %s: coupon code */
            $c_code = $parm->get_code() ;

            if ( preg_match('/\s/', $c_code) ){
                $err = sprintf( __( 'NO SPACE REQUIRED.. TRY AGAIN WITH NO SPACE', 'woocommerce' ), esc_html( $parm->get_code() ) );
            }else{
                $err = sprintf( __( 'Coupon "%s" does not exist!', 'woocommerce' ), esc_html( $parm->get_code() ) );
            }
            break;

        case 106:
             $err = sprintf( __( 'Coupon has been redeemed Already', 'woocommerce' ), esc_html( $parm->get_code() ) );
            break;

 }
return $err;
}


/*REMOVE AUTOFILL WHEN CHECKOUT PAGE IS LOADED*/
/*add_filter('woocommerce_checkout_get_value','__return_empty_string', 1, 1);*/

/*DEFAULT CHECKOUT STATE*/
add_filter( 'default_checkout_billing_state', 'change_default_checkout_state' );
add_filter( 'default_checkout_shipping_state', 'change_default_checkout_state' );
function change_default_checkout_state() {
    return ''; //set default state
}