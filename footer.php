<?php
global $porto_settings, $porto_layout;

$default_layout = porto_meta_default_layout();
$wrapper        = porto_get_wrapper_type();
?>
		<?php get_sidebar(); ?>

		<?php if ( porto_get_meta_value( 'footer', true ) ) : ?>

			<?php

			$cols = 0;
			for ( $i = 1; $i <= 4; $i++ ) {
				if ( is_active_sidebar( 'content-bottom-' . $i ) ) {
					$cols++;
				}
			}

			if ( is_404() ) {
				$cols = 0;
			}

			if ( $cols ) :
				?>
				<?php if ( 'boxed' == $wrapper || 'fullwidth' == $porto_layout || 'left-sidebar' == $porto_layout || 'right-sidebar' == $porto_layout ) : ?>
					<div class="container sidebar content-bottom-wrapper">
					<?php
				else :
					if ( 'fullwidth' == $default_layout || 'left-sidebar' == $default_layout || 'right-sidebar' == $default_layout ) :
						?>
					<div class="container sidebar content-bottom-wrapper">
					<?php else : ?>
					<div class="container-fluid sidebar content-bottom-wrapper">
						<?php
					endif;
				endif;
				?>

				<div class="row">

					<?php
					$col_class = array();
					switch ( $cols ) {
						case 1:
							$col_class[1] = 'col-md-12';
							break;
						case 2:
							$col_class[1] = 'col-md-12';
							$col_class[2] = 'col-md-12';
							break;
						case 3:
							$col_class[1] = 'col-lg-4';
							$col_class[2] = 'col-lg-4';
							$col_class[3] = 'col-lg-4';
							break;
						case 4:
							$col_class[1] = 'col-lg-3';
							$col_class[2] = 'col-lg-3';
							$col_class[3] = 'col-lg-3';
							$col_class[4] = 'col-lg-3';
							break;
					}
					?>
						<?php
						$cols = 1;
						for ( $i = 1; $i <= 4; $i++ ) {
							if ( is_active_sidebar( 'content-bottom-' . $i ) ) {
								?>
								<div class="<?php echo esc_attr( $col_class[ $cols++ ] ); ?>">
									<?php dynamic_sidebar( 'content-bottom-' . $i ); ?>
								</div>
								<?php
							}
						}
						?>

					</div>
				</div>
			<?php endif; ?>

			</div><!-- end main -->
			<?php if( is_single()  && 'post' == get_post_type() ): ?>
				<?php echo do_shortcode( '[porto_block name="blog-content-bottom"]' ); ?>
			<?php endif; ?>
			<?php
			do_action( 'porto_after_main' );
			$footer_view = porto_get_meta_value( 'footer_view' );
			?>

			<div class="footer-wrapper<?php echo 'wide' == $porto_settings['footer-wrapper'] ? ' wide' : '', $footer_view ? ' ' . esc_attr( $footer_view ) : '', isset( $porto_settings['footer-reveal'] ) && $porto_settings['footer-reveal'] ? ' footer-reveal' : ''; ?>">

				<?php if ( porto_get_wrapper_type() != 'boxed' && 'boxed' == $porto_settings['footer-wrapper'] ) : ?>
				<div id="footer-boxed">
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer-top' ) && ! $footer_view ) : ?>
					<div class="footer-top">
						<div class="container">
							<?php dynamic_sidebar( 'footer-top' ); ?>
						</div>
					</div>
				<?php endif; ?>

				<?php
					get_template_part( 'footer/footer' );
				?>

				<?php if ( porto_get_wrapper_type() != 'boxed' && 'boxed' == $porto_settings['footer-wrapper'] ) : ?>
				</div>
				<?php endif; ?>

			</div>

		<?php else : ?>

			</div><!-- end main -->

			<?php
			do_action( 'porto_after_main' );
		endif;
		?>

		<?php if ( 'side' == porto_get_header_type() ) : ?>
			</div>
		<?php endif; ?>

	</div><!-- end wrapper -->
	<?php do_action( 'porto_after_wrapper' ); ?>

<?php

if ( isset( $porto_settings['mobile-panel-type'] ) && 'side' === $porto_settings['mobile-panel-type'] && 'overlay' != $porto_settings['menu-type'] ) {
	// navigation panel
	get_template_part( 'panel' );
}

?>

<!--[if lt IE 9]>
<script src="<?php echo esc_url( PORTO_JS ); ?>/libs/html5shiv.min.js"></script>
<script src="<?php echo esc_url( PORTO_JS ); ?>/libs/respond.min.js"></script>
<![endif]-->

<!-- FOR ADS PIXEL -->
<?php 

	if ( is_front_page() || is_single() || is_product() ){ ?>
		
		<img height="1" width="1" style="border-style:none;" alt="" src="https://insight.adsrvr.org/track/pxl/?adv=em7hok2&ct=0:1q5yzs6&fmt=3"/>
	
	<?php } else if ( is_cart() ) { ?>

		<img height="1" width="1" style="border-style:none;" alt="" src="https://insight.adsrvr.org/track/pxl/?adv=em7hok2&ct=0:yaldq3g&fmt=3"/>

	<?php } else if ( is_checkout() ) { ?>

		<img height="1" width="1" style="border-style:none;" alt="" src="https://insight.adsrvr.org/track/pxl/?adv=em7hok2&ct=0:3ywpgz4&fmt=3"/>
		
	<?php } else if ( is_wc_endpoint_url( 'order-received' ) ) { ?>

		<img height="1" width="1" style="border-style:none;" alt="" src="https://insight.adsrvr.org/track/pxl/?adv=em7hok2&ct=0:z958qcn&fmt=3"/>

	<?php } ?>
<?php wp_footer(); ?>


<?php $user_id = get_current_user_id(); ?>

<script>
	var if_logged_in = "<?php echo $user_id; ?>";
	var urlx = window.location.href;
	if ( if_logged_in > 0) {
		jQuery("#nav-menu-item-2047, #nav-menu-item-4632").hide();

	}else{
		jQuery('#nav-menu-item-4386').hide();
		if ( urlx == "https://blosumcbd.com/my-account/") {
			window.location.href = 'https://blosumcbd.com/sign-in/';
		}
	}

	/*HIDE COUPON FIELD IF RECURRING ORDERS*/ 

	if ( jQuery('.recurring-total')[0] || jQuery('.recurring-totals')[0] ) {
		jQuery('.woocommerce .cart-collaterals .coupon').hide();
		jQuery('#payment .woocommerce-info').hide();
	}
	setTimeout(function(){
		if ( jQuery('.recurring-total')[0] || jQuery('.recurring-totals')[0] ) {
			jQuery('.woocommerce .cart-collaterals .coupon').hide();
			jQuery('#payment .woocommerce-info').hide();
		}
	},2000);
	

	/*CART TOOGLE SWITCH*/
	var btnclass = '.cart-switch-field input[type="radio"]';

	jQuery(document).on('click', btnclass, function(){
		jQuery('.cart-switch-field label').removeClass('selected');
		jQuery('.cart-switch-field input[type="radio"]:checked').parent().addClass('selected');

		var chkSubscribe = jQuery('.cart-switch-field input[type="radio"]:checked').val();
		if ( chkSubscribe == "1_month" ) {
			jQuery('.woocommerce .cart-collaterals .coupon').hide();
		}else{
			jQuery('.woocommerce .cart-collaterals .coupon').show();
		}
	});
	
	jQuery('.cart-switch-field input[type="radio"]:checked').parent().addClass('selected');


	jQuery('.products .add_to_cart_button.ajax_add_to_cart').on('click', function(){
		jQuery(this).html('<i class="fa fa-spinner fa-spin"></i>').addClass('added_to_cart_1');

		setTimeout(function(){
			jQuery('.added_to_cart_1').html('ADDED TO CART');
	    	jQuery('.added_to_cart_1').attr('href','javascript:void(0)');
		},2000);
	});
	jQuery(document).ajaxComplete(function() {


		if ( jQuery('.recurring-total')[0] || jQuery('.recurring-totals')[0] ) {
			jQuery('.woocommerce .cart-collaterals .coupon').hide();
			jQuery('#payment .woocommerce-info').hide();
		}
		setTimeout(function(){
			if ( jQuery('.recurring-total')[0] || jQuery('.recurring-totals')[0] ) {
				jQuery('.woocommerce .cart-collaterals .coupon').hide();
				jQuery('#payment .woocommerce-info').hide();
			}
		},2000);
		
		

		/*CART TOOGLE SWITCH*/
		jQuery('.cart-switch-field .price.subscription-price').html('YES');
		jQuery('.cart-switch-field .one-time-option span.one-time-option-details').html('NO');
		jQuery('.cart-switch-field input[type="radio"]:checked').parent().addClass('selected');

		jQuery('.xoo-wsc-price span span.subscription-details').html('');
		jQuery('.xoo-wsc-price span span.subscription-price span.subscription-details').html('<br>Monthly Auto Ship Subscription');

	});

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
        jQuery('a').off('menufocus hover mouseover');
		jQuery('a.add_to_cart_button').on('touchend', function(e) {
			jQuery(this).trigger('click');
			e.preventDefault();
		});
    }
    jQuery('a.add_to_cart_button').addClass('no-touch');


</script>

<?php
// js code (Theme Settings/General)
if ( isset( $porto_settings['js-code'] ) && $porto_settings['js-code'] ) {
	?>
	<script>
		<?php echo porto_filter_output( $porto_settings['js-code'] ); ?>
	</script>
<?php } ?>
<?php if ( isset( $porto_settings['page-share-pos'] ) && $porto_settings['page-share-pos'] ) : ?>
	<div class="page-share position-<?php echo esc_attr( $porto_settings['page-share-pos'] ); ?>">
		<?php get_template_part( 'share' ); ?>
	</div>
<?php endif; ?>



<script type="text/javascript">
	//moving toc in sidebar on blog single page
	var $ = jQuery;
	if($('.post-template-default.single-post #ezw_tco-2').length){
		$(window).on('load resize', function(){
		  if ($(window).width() <= 991) {
		  	var block = $('#ezw_tco-2').detach();
			  block.insertAfter('h1.entry-title');
		  }
		  else{
		  	var block = $('#ezw_tco-2').detach();
			  block.insertBefore('aside#custom_html-5');
		  }
		});
	}

   $('.ez-toc-list .ez-toc-link').on('click', function(){
      var url_hash = $(this).attr('href');
      window.location.hash = url_hash;
   });
	
</script>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(65300113, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/65300113" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

</body>
</html>
